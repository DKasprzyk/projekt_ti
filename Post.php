<?php
	session_start();

	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>	
	<title>Post</title>	
	<?php 
		require_once('headStatic.php');
	?>
</head>
	<body>
		<?php 
			require_once('nav.php');
		?>

		<br>
		<br>
		<div class = "box_naj">
			<?php 
				require_once('dane.php');
				getPostDetailsData($_GET['id']);
			?>
		</div>

		<?php 
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>