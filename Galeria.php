<?php
	session_start();
?>
<!DOCTYPE HTML>
<html>
<head>	
	<title>Galeria</title>	
	<?php 
		require_once('headStatic.php');
	?>
</head>
	<body>
		<?php 
			require_once('nav.php');
		?>

		<section class ="form">
			<div class="container">
				<div class="row">
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Jakub_Bucki" target = "_blank"><img src="img/jb.png" alt="Jakub Bucki"></a>
								<figcaption>Jakub Bucki</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Mohammed_Al_Hachdadi" target = "_blank"><img src="img/mah.png" alt="Mohammed Al Hachdadi"></a>
								<figcaption>Mohammed Al Hachdadi</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3 ">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Jakub_Popiwczak" target = "_blank"><img src="img/jp.png" alt="Jakub Popiwczak"></a>
								<figcaption>Jakub Popiwczak</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Szymon_Biniek" target = "_blank"><img src="img/sb.png" alt="Szymon Biniek"></a>
								<figcaption>Szymon Biniek</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/%C5%81ukasz_Wi%C5%9Bniewski" target = "_blank"><img src="img/lw.png" alt="Łukasz Wiśniewski"></a>
								<figcaption>Łukasz Wiśniewski</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Jurij_H%C5%82adyr" target = "_blank"><img src="img/jg.png" alt="Juri Gladyr"></a>
								<figcaption>Juri Gladyr</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Micha%C5%82_Szalacha" target = "_blank"><img src="img/msz.png" alt="Michał Szalacha"></a>
								<figcaption>Michał Szalacha</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://www.plusliga.pl/players/id/2100506.html" target = "_blank"><img src="img/pcd.png" alt="Patryk Cichosz-Dzyga"></a>
								<figcaption>Patryk Cichosz-Dzyga</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Lukas_Kampa" target = "_blank"><img src="img/lk.png" alt="Lukas Kampa"></a>
								<figcaption>Lukas Kampa</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Eemi_Tervaportti" target = "_blank"><img src="img/et.png" alt="Eemi Tervaportti"></a>
								<figcaption>Eemi Tervaportti</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Yacine_Louati" target = "_blank"><img src="img/yl.png" alt="Yacine Louati"></a>
								<figcaption>Yacine Louati</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://www.plusliga.pl/players/id/2100287/tour/2020.html" target = "_blank"><img src="img/mg.png" alt="Michał Geirżot"></a>
								<figcaption>Michał Geirżot</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Stanis%C5%82aw_Wawrzy%C5%84czyk" target = "_blank"><img src="img/sw.png" alt="Stanisław Wawrzyńczyk"></a>
								<figcaption>Stanisław Wawrzyńczyk</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Tomasz_Fornal" target = "_blank"><img src="img/tf.png" alt="Tomasz Fornal"></a>
								<figcaption>Tomasz Fornal</figcaption>
							</figure>
						
						</div>
						
						<div class="col-sm-6 col-md-3">
						
							<figure>
								<a href="https://pl.wikipedia.org/wiki/Rafa%C5%82_Szymura" target = "_blank"><img src="img/rsz.png" alt="Rafał Szymura"></a>
								<figcaption>Rafał Szymura</figcaption>
							</figure>
						
						</div>
						
						
						
						
				
				</div>
			</div>
		</section>	
		<br>

		<?php 
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>