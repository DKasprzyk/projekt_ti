<?php
	session_start();

	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>
<!DOCTYPE HTML>
<html>
<head>	
	<title>Strefa Kibica - Jastębie: konto użytkownika</title>	
	<?php 
		require_once('headStatic.php');
	?>
</head>
	<body>
		<?php 
			require_once('nav.php');
		?>
		
		<section class="form">
			<?php
				echo "<p>Witaj ".$_SESSION['user']."</br>"  .'[ <a href="logout.php">Wyloguj się!</a> ]</p>';
			?>
		</section>

		<?php 
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>