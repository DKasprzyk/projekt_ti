<?php
session_start();
	
if (!isset($_SESSION['zalogowany']) || $_SESSION['user'] != 'root')
{
	header('Location: index.php');
	exit();
}
else
{
    require('connect.php');
    $mysqli = @new mysqli($host, $db_user, $db_password, $db_name);

    if(!$mysqli)
    {
        echo'("Wystąpił błąd podaczas połączenia z bazą: ")';
        exit;
    }

    if($_FILES['zdjecie']['tmp_name'] != null)
    {
        $image = $_FILES['zdjecie']['tmp_name'];
        $img = file_get_contents($image);
        $stmt = $mysqli->prepare("INSERT INTO posty (id, tresc, tytul, zdjecie) VALUES (NULL, ?, ?, ?);");
        $stmt->bind_param("sss", $_POST['tresc'], $_POST['tytul'], $img);
    }
    else
    {
        $stmt = $mysqli->prepare("INSERT INTO posty (id, tresc, tytul, zdjecie) VALUES (NULL, ?, ?, (select zdjecia from zdjecia where id_zdjecia = 1));");
        $stmt->bind_param("ss", $_POST['tresc'], $_POST['tytul']);
    }    
    $stmt->execute();
    $mysqli->close();
    header('Location: Aktualnosci.php');
}
?>