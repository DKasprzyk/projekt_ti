<?php
session_start();
	
if (!isset($_SESSION['zalogowany']) || $_SESSION['user'] != 'root')
{
	header('Location: index.php');
	exit();
}
else
{
    require('connect.php');
    $mysqli = @new mysqli($host, $db_user, $db_password, $db_name);

    if(!$mysqli)
    {
        echo'("Wystąpił błąd podaczas połączenia z bazą: ")';
        exit;
    }
    
    if($_POST['tresc'] != null)
    {
        $stmt1 = $mysqli->prepare("UPDATE posty SET tresc = ? WHERE id = ?;");
        $stmt1->bind_param("si", $_POST['tresc'], $_POST['id']); 
        $stmt1->execute();
    }

    if($_POST['tytul'] != null)
    {
        $stmt2 = $mysqli->prepare("UPDATE posty SET tytul = ? WHERE id = ?;");
        $stmt2->bind_param("si", $_POST['tytul'], $_POST['id']); 
        $stmt2->execute();
    }

    if($_FILES['zdjecie']['tmp_name'] != null)
    {
        $image = $_FILES['zdjecie']['tmp_name'];
        $img = file_get_contents($image);
        $stmt3 = $mysqli->prepare("UPDATE posty SET zdjecie = ? WHERE id = ?;");
        $stmt3->bind_param("si", $img, $_POST['id']); 
        $stmt3->execute();
    }
    
    $mysqli->close();
    header('Location: Aktualnosci.php');
}
?>