<?php
	session_start();
?>
<!DOCTYPE HTML>
<html>
<head>	
	<title>RODO</title>	
	<?php 
		require_once('headStatic.php');
	?>
</head>
	<body>
		<?php 
			require_once('nav.php');
		?>

		<section class ="form">
			<div class="container">
				
				<header>
					<div class="text_post2 text-center p-3">
						<h1>Informacja o RODO<h1>
					</div>
				<header>	
				<section>
					<article id="RODO_tresc" class="text_post2">
						<p style="text-align: justify;"><span style="font-size: 11pt;"><b>Klub Kibica Siatkówki Jastrzębie</b></span></p>
						<p style="text-align: justify;"><span style="font-size: 11pt;">Klauzula informacyjna dotycząca przetwarzania danych osobowych przez Klub Kibica Siatkówki Jastrzębie.</span></p>
						<p style="text-align: justify;"><span style="font-size: 11pt;">1. Administratorem Państwa danych osobowych jest stowarzyszenie Klub Kibica Siatkówki Jastrzębie z siedzibą w Jastrzębiu – Zdroju ul. Mikołaja Reja 10 zarejestrowane w Rejestrze Stowarzyszeń Krajowego Rejestru Sądowego pod numerem KRS 000011235813.</span></p>
						<p style="text-align: justify;"><span style="font-size: 11pt;">2. Administrator wyznaczył Inspektora Ochrony Danych – jest nią Wiceprezes Zarządu Stowarzyszenia z którym może się Pani/Pan skontaktować telefonicznie pod numerem 106-850-222 lub pisemnie na adres: klub_kibica@jastrzebiekks.com „Dane osobowe”. Z inspektorem ochrony danych można się kontaktować we wszystkich sprawach dotyczących przetwarzania danych osobowych oraz korzystania z praw związanych z przetwarzaniem danych.</span></p>
						<p style="text-align: justify;"><span style="font-size: 11pt;">3. Pani/Pana dane osobowe mogą być przekazywane naszym podwykonawcom wyłącznie w celu realizacji naszych zadań oraz organom ścigania (sąd, komornik, policja) na podstawie odpowiedniego nakazu.</span></p>
						<p style="text-align: justify;"><span style="font-size: 11pt;">4. Od 25 maja 2018 r. podstawą prawną przetwarzania danych osobowych znajdujących się w naszej bazie jest art. 6 ust. 1 pkt f Rozporządzenia Parlamentu Europejskiego i Rady (UE) 2016/679 z 7 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (RODO), tj. prawnie uzasadniony interes realizowany przez administratora danych. Oznacza to, że dane osobowe z ww. bazy będą przetwarzane przede wszystkim w celu wysyłania zaproszeń i informacji, realizacji projektów, także w celach analitycznych, statystycznych i kontrolnych oraz ewentualnego ustalenia i dochodzenia roszczeń oraz obrony przed roszczeniami.</span></p>
						<p style="text-align: justify;"><span style="font-size: 11pt;">5. Dane osobowe będą przetwarzane do czasu zgłoszenia sprzeciwu, natomiast dane osobowe uczestników projektów oferowanych przez Klub Kibica Siatkówki Jastrzębie przechowywane przez okres 10 lat od ich zakończenia. Okres ten może zostać przedłużony do czasu upływu okresu przedawnienia ewentualnych roszczeń, jeżeli przetwarzanie danych osobowych będzie niezbędne do dochodzenia lub obrony przed takimi roszczeniami.</span></p>
						<p style="text-align: justify;"><span style="font-size: 11pt;">6. Przysługuje Pani/Panu prawo dostępu do swoich danych, sprostowania i usunięcia lub ograniczenia przetwarzania.</span></p>
						<p style="text-align: justify;"><span style="font-size: 11pt;">7. W każdym czasie może Pani/Pan wnieść skargę do organu nadzorującego przestrzeganie przepisów o ochronie danych osobowych, jeżeli uzna Pani/Pan, że przetwarzanie Pani/Pana danych narusza RODO.</span></p>
						<p style="text-align: justify;"><span style="font-size: 11pt;">8. Powyższe informacje dotyczą przetwarzania Pani/Pana danych osobowych, znajdujących się w bazie osób uczestników szkoleń, wyjazdów, projektów na organizowane przez nas lub naszych Sponsorów wydarzenia w celu wskazanym powyżej i informowania o inicjatywach, podejmowanych przez Klub Kibica Siatkówki Jastrzębie.</span></p>
					</article>
				</section>
				</div>
				</section>
			</div>
		</section>

		<?php 
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>