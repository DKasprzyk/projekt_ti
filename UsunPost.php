<?php
session_start();
	
if (!isset($_SESSION['zalogowany']) || $_SESSION['user'] != 'root')
{
	header('Location: index.php');
	exit();
}
else
{
    require_once('dane.php');
    deletePostData($_GET['id']);
    header('Location: Aktualnosci.php');
}
?>