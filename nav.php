<header>
	<nav class=" navbar navbar-light navbar-expand-lg nav-1">
		<img src="img/jastzrebski.png" width="70" height="50" class="d-inline-block mr-1 align-bottom" alt="">

		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainmenu" aria-controls="mainmenu" aria-expanded="false" aria-label="Przęłącznik nawigacji">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		<div class="collapse navbar-collapse" id="mainmenu">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">									
					<a class="nav-link" href="index.php">Strona Główna</a>
				</li>

				<li class="nav-item">									
					<a class="nav-link" href="Aktualnosci.php">Aktualności</a>
				</li>

				<li class="nav-item">									
					<a class="nav-link " href="https://www.plusliga.pl/table.html" role="button" aria-expanded="false" id="submenu" aria-haspopup="true">Wyniki</a>
				</li>
				
				<li class="nav-item">									
					<a class="nav-link" href="Galeria.php">Galeria</a>
				</li>
				
				<li class="nav-item">									
					<a class="nav-link" href="RODO.php">RODO</a>
				</li>

				<li class="nav-item">									
					<a class="nav-link" href="Kontakt.php">Kontakt</a>
				</li>

				<li class="nav-item">									
					<a class="nav-link" href="index.php">Zaloguj</a>
				</li>

				<?php if (isset($_SESSION['zalogowany'])) {?>
					<li class="nav-item">									
						<?php
							echo "<p>Witaj ".$_SESSION['user']."</br>"  .'[ <a href="logout.php">Wyloguj się!</a> ]</p>';
						?>
					</li>
				<?php }?>
			</ul>
		</div>
	</nav>
</header>

