<?php
session_start();
	
if (!isset($_SESSION['zalogowany']) || $_SESSION['user'] != 'root')
{
	header('Location: index.php');
	exit();
}

?>

<!DOCTYPE HTML>
<html>
	<head>	
        <title>Edycja posta</title>   
		<?php 
			require_once('headStatic.php');
		?>
	</head>
	<body>
		<?php 
			require_once('nav.php');
        ?>
            
            <form action="EdytujPostExec.php" class="form" method="post" enctype="multipart/form-data">
                <label>Tytul<input type="text" name="tytul" value="<?php print $_GET["tytul"]?>"></label>
                <label>Tresc<input type="text" name="tresc" value="<?php print $_GET["tresc"]?>"></label>
                <label>Zdjecie <input type="file" name="zdjecie"></label>
                <input type="hidden" name="id" value="<?php print $_GET["id"]?>">
                <input type="submit">
            </form>

        <?php
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>