<?php
	session_start();
?>
<!DOCTYPE HTML>
<html>
<head>	
	<title>Kontakt</title>	
	<?php 
		require_once('headStatic.php');
	?>
</head>
	<body>
		<?php 
			require_once('nav.php');
		?>

		<section class ="form">
			<div class="container">
				<br>
				<div id="mapa">
					<div id="mapa1">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4313.758769609842!2d18.618335170649143!3d49.99305359171465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471152972cab7921%3A0x5a38db47448cb8d3!2sMiko%C5%82aja%20Reja%2010%2C%2044-268%20Jastrz%C4%99bie-Zdr%C3%B3j!5e0!3m2!1spl!2spl!4v1609861526778!5m2!1spl!2spl" width="700" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					</div>
				
					<div id="kontakt" >
						<p>
						<br>
						<br>
						<br>
						<br>
						Sekretariat: <a href="mailto:administracja@jastrzebskiwegiel.pl">administracja@jastrzebskiwegiel.pl</a>
						<br>
						<br>
						<br>Hala Widowiskowo-Sportowa
						<br>(Biuro KS Jastrzębski Węgiel S.A.)
						<br>
						<br>
						<br>Al. Jana Pawła II 6
						<br>44-335 Jastrzębie-Zdrój
						<br>Tel.: 32 476 48 95
					
						</p>
					</div>
					
				</div>
				
				
				</div>
		</section>	
		<br>

		<?php 
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>