-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 06 Sty 2021, 19:17
-- Wersja serwera: 10.4.11-MariaDB
-- Wersja PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `klub_kibica`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(64) COLLATE utf8mb4_polish_ci NOT NULL,
  `pass` varchar(64) COLLATE utf8mb4_polish_ci NOT NULL,
  `email` tinytext COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `user`, `pass`, `email`) VALUES
(1, 'root', 'jastrzebie', ''),
(2, 'none', 'qwertyui', 'none@gmail.com'),
(3, 'qwerty', '$2y$10$ToxDCcEEBU5hAI9KFhw8RObdwKMigKxQkt./LaedjpNRE1fnMbRwK', 'sztywny.zimny99@gmail.com'),
(3, 'abc', '$2y$10$2/5cD8dAXkDxfJ/flQ6H3uAPUUBcWnaZfiwc613g5qpG8lleny.iC', 'abc@onet.pl'),
(3, 'abcd', '$2y$10$bVZTG1BmqLRuBcNK6xTAruTADR1k7zHmfpZ.TdX4.tq17KecsiQU6', 'abcd@gmail.com'),
(3, 'abcde', '$2y$10$SPvOIlb./AVrDfN1yDnBRuOkVCJk1lDH0XYi6XHM9.roh/DuGAOq.', 'a@'),
(3, 'abcdef', '$2y$10$li0nu4FkYLIf3gZulqT/FetstYUJUF3bCsyobHpTxCab0I.ozLMRK', 'aa@'),
(3, 'abcdefg', '$2y$10$j37MM117tMN2IJI1CJln9eNbce4yqLoRj6uUYQfA.NMcJrclaIi4W', 'aa'),
(3, '', '$2y$10$au2SKClfA4hqXLjsN/.zzuRDTLJcBKu1az64pa5zRd1T9faeCXoDm', 'aa@q'),
(3, 'abcdaazxZxzx', '$2y$10$.PFFaT3WPqTD/71Qa3mKduzMJ3On3ktXybkzjDq4HWnTVUFz2ld0a', 'aaaa@'),
(3, 'aaaa', '$2y$10$iM97abMMNcFoZ3Dk/XfCe.wMifsLETgmflwABQTuu8/nuwOg7x4SG', 'aaa@'),
(12, 'aq', '$2y$10$eFYeQCtdK71NJt1vnfUxGePPu1w3wkF5aPgECOiAEYKSrBKMv.Rb6', 'aq@'),
(12, 'abcdefgq', '$2y$10$RHxqCiIOKgfTSmyhYdbkyOKILRv2NoaFdm.ioQOyRRB.FBynUG42y', 'qq@'),
(12, 'qqqqq', '$2y$10$HrwHZLmsgFAG7no1mXS6ZOpHOQAWluHMlySDWbHTgrnh9tNN8ARiK', 'qqqqq@'),
(12, 'qwertyqq', '$2y$10$UpUrIu9g6ErgqeFOmFOwD.zK.uofIwBln7WEC0nhunCpR/NELvbqq', 'qqq@'),
(12, 'qaz', '$2y$10$vo1kIcKRTQscYxFr49/hje52tcv8az.rA8diEEZErTEQ2UZHqLA4S', 'qaz@'),
(12, 'zzz', '$2y$10$DE5W7g2Gxu43DHig4y/UfeZaxbFrx9LGWFVjzwzZrLru9bIsnT8x6', 'zzz@'),
(12, 'zazaza', '$2y$10$Lc/uPBrImMVTCs/Bjgj6senuPYQpiH5t0xoX8DtHcH6KqV0A5K33W', 'zaaza@'),
(12, 'klklkl', '$2y$10$bWJI9v6vxIVT.s3fCLp9F.WXOY7ehC2h202v0ddOkm.VWhYe9sEqW', 'klklkl@');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
