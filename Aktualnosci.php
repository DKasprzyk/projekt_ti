<?php
	session_start();
?>
<!DOCTYPE HTML>
<html>
<head>	
	<title>Aktualności</title>	
	<?php 
		require_once('headStatic.php');
	?>
</head>
	<body>
		<?php 
			require_once('nav.php');
		?>

		<div class = "box_naj">
			<div class = "box">
				<section class="posty">
					<br>
					<div class="container">
						<?php 
							require_once('dane.php');
							getPostsData(10000);
						?>
					</div>	
					<br>
				</section>
		
			</div>
			
			<div class = "blok">
				<p class = "naglowek">NASI SPONSORZY</p>		
				<br>
				<ul id = "ul1">
					
					<li class="li1"><a href='https://www.jsw.pl/' target = "_blank"> <img id = "obrazek" src="img/jsw1.png" > </a></li>
					<li class="li1"><a href='https://www.jastrzebie.pl/' target = "_blank"><img id = "obrazek" src="img/miasto1.png" ></a> </li>
					<li class="li1"><a href='https://www.jzr.pl/' target = "_blank"><img id = "obrazek" src="img/jzr.png" ></a> </li>
					<li class="li1"><a href='https://www.jsk.pl/' target = "_blank"><img id = "obrazek" src="img/jsk.png" ></a> </li>
					<li class="li1"><a href='https://www.jsu.pl/' target = "_blank"><img id = "obrazek1" src="img/jsw2.png" ></a> </li>
				</ul>
				<br>	
			</div>
		</div>

		<?php 
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>