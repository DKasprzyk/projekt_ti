<?php session_start()?>
<!DOCTYPE HTML>
<html>
	<head>	
		<?php 
			require_once('headStatic.php');
			if ((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']==true))
			{
				echo '<title>Strefa Kibica - Jastębie</title>';
			}
			else 
			{
				echo '<title>Strona główna</title>';
				echo '<link rel="stylesheet" href="styl_2.css" type="text/css" />';
			}
		?>
	</head>
	<body>
		<?php 
			require_once('nav.php');

			if ((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']==true))
			{
				require_once('mainContent.php');
			}
			else
			{
				require_once('login.php');
			}

			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>