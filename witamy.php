<?php
session_start();
	
if (!isset($_SESSION['all_right']))
{
	header('Location: index.php');
	exit();
}
else
{
	unset($_SESSION['all_right']);
}
	
	if (isset($_SESSION['fr_nick'])) 	unset($_SESSION['fr_nick']);
	if (isset($_SESSION['fr_email'])) 	unset($_SESSION['fr_email']);
	if (isset($_SESSION['fr_haslo1'])) 	unset($_SESSION['fr_haslo1']);
	if (isset($_SESSION['fr_haslo2'])) 	unset($_SESSION['fr_haslo2']);
	if (isset($_SESSION['fr_regulamin'])) unset($_SESSION['fr_regulamin']);
	if (isset($_SESSION['e_nick'])) 	unset($_SESSION['e_nick']);
	if (isset($_SESSION['e_email'])) 	unset($_SESSION['e_email']);
	if (isset($_SESSION['e_haslo'])) 	unset($_SESSION['e_haslo']);
	if (isset($_SESSION['e_regulamin'])) unset($_SESSION['e_regulamin']);
	if (isset($_SESSION['e_bot'])) 		unset($_SESSION['e_bot']);
	
?>

<!DOCTYPE HTML>
<html>
<head>	
	<title>Strefa Kibica - Jastębie</title>	
	<?php 
		require_once('headStatic.php');
	?>
</head>
	<body>
		<?php 
			require_once('nav.php');
		?>

		<section class="form" >
			<p>
				<h3>Dziękujemy za dołączenie do klubu kibica Jastrzębia.
				<br>Od tej pory możesz w pełni korzystać z naszego serwisu jak i zalogowac się na swoje konto.</h3>\
			</p>
		</section>
		<a style="color:black" href="index.php"><h5>Zaloguj się na swoje konto!</h5></a>
		<br/><br/>

		<?php 
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>