<footer class="footer text-center text-lg-start nav-1">
		
		<div class="container p-4">
		  
		  <div class="row">
			
			<div class="col-lg-6 col-md-12 mb-4 mb-md-0">
			  <h5 class="text-uppercase">Klub Sportowy Jastrzębski Węgiel S.A.</h5>
	  
			  <p>
				 
				ul. Reja 10 </br>
				44-268 Jastrzębie Zdrój </br></br>
				NIP: 6332118781 </br>
				Regon: 240072309 </br>
				KRS: 0000233145 </br>
			  </p>
			</div>
			
			<div class="col-lg-3 col-md-6 mb-4 mb-md-0">
	  
			  <ul class = "li2">
				<li>
				  <a class="text-dark" href="index.php">Strona Główna</a>
				</li>
				<li>
				  <a class="text-dark" href="Aktualnosci.php">Aktualności</a>
				</li>
				<li>
				  <a class="text-dark" href="https://www.plusliga.pl/table.html" role="button" aria-expanded="false" id="submenu" aria-haspopup="true">Wyniki</a>
				</li>
				<li>
				  <a class="text-dark" href="Galeria.php">Galeria</a>
				</li>
				<li>
				  <a class="text-dark" href="RODO.php">RODO</a>
				</li>
				<li>
				  <a class="text-dark" href="Kontakt.php">Kontakt</a>
				</li>
				<li>
				  <a class="text-dark" href="index.php">Zaloguj</a>
				</li>
			  </ul>
			</div>

			<div class="col-lg-3 col-md-6 mb-4 mb-md-0">
				<img id = "obrazek" src="img/jastzrebski.png" style ="width: 200px; height: 150px" >
			</div>
			
		  
		  
		</div>
		
	  
		
		<div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
		  © Klub Sportowy Jastrzębski Węgiel S.A. 2000 - 2022
		 
		</div>
		
	</footer>