<?php
    function getPostsData($max_rows = 6)
	{
		require('connect.php');
		$mysqli = @new mysqli($host, $db_user, $db_password, $db_name);

		if(!$mysqli)
		{
			echo'("Wystąpił błąd podaczas połączenia z bazą: ")';
			exit;
		}

		$sql = "SELECT tytul, tresc, zdjecie, id FROM posty order by id desc limit $max_rows";
        $result = $mysqli->query($sql);


        echo '<div class="row">';
        if(isset($_SESSION['zalogowany']) && $_SESSION['user'] == 'root') {
            echo '<div class="col-sm-6 col-md-4 custom-post-padding"><figure>';
            echo '<a href="DodajPost.php"><img src="img/add.jpg"/></a>';
            echo '</figure></div>';
        }
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<div class="col-sm-6 col-md-4 custom-post-padding"><figure>';
            echo '<a href="Post.php?id='.$row['id'].'"><img src="data:image/jpeg;base64,'.base64_encode($row['zdjecie']).'"/></a>';
            echo '<h4 class="text_post">'.$row['tytul'].'</h4>';
            echo '</figure>';
            if(isset($_SESSION['zalogowany']) && $_SESSION['user'] == 'root') {
                echo '<a class="btn btn-danger ico-box" href="UsunPost.php?id='.$row['id'].'"><i class="fas fa-trash-alt"></i></a>';
                echo '<a class="btn btn-primary ico-box" href="EdytujPost.php?id='.$row['id'].'&tytul='.$row['tytul'].'&tresc='.$row['tresc'].'"><i class="fas fa-edit"></i></a>';
            }
            echo '</div>';
        }
        
        echo '</div>';
        
        $result->free();
        $mysqli->close();
    }
    
    function getPostDetailsData($id = 1)
	{
		require('connect.php');
		$mysqli = @new mysqli($host, $db_user, $db_password, $db_name);

		if(!$mysqli)
		{
			echo'("Wystąpił błąd podaczas połączenia z bazą: ")';
			exit;
		}

		$sql = "SELECT tytul, tresc, zdjecie FROM posty where id=$id";
        $result = $mysqli->query($sql);

        
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<div class="tresc_posta">';
            echo '<h1>'.$row['tytul'].'</h1>';
            echo '<img class="obrazek_post" src="data:image/jpeg;base64,'.base64_encode($row['zdjecie']).'"/>';
            echo '<br><br>';
            echo '<p>'.$row['tresc'].'</p>';
            echo '<br><br>';
            echo '</div>';
        }
        $result->free();
        $mysqli->close();
    }
    
    function deletePostData($id = 1)
	{
		require('connect.php');
		$mysqli = @new mysqli($host, $db_user, $db_password, $db_name);

		if(!$mysqli)
		{
			echo'("Wystąpił błąd podaczas połączenia z bazą: ")';
			exit;
		}

		$sql = "DELETE FROM posty WHERE id=$id";
        $mysqli->query($sql);
        $mysqli->close();
    }
?>

