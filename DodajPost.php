<?php
session_start();
	
if (!isset($_SESSION['zalogowany']) || $_SESSION['user'] != 'root')
{
	header('Location: index.php');
	exit();
}

?>

<!DOCTYPE HTML>
<html>
	<head>	
        <title>Dodawanie posta</title>   
		<?php 
			require_once('headStatic.php');
		?>
	</head>
	<body>
		<?php 
			require_once('nav.php');
        ?>
            
            <form action="DodajPostExec.php" class="form" method="post" enctype="multipart/form-data">
                <label>Tytul<input type="text" name="tytul"></label>
                <label>Tresc<input type="text" name="tresc"></label>
                <label>Zdjecie <input type="file" name="zdjecie"></label>
                <input type="submit">
            </form>

        <?php
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>