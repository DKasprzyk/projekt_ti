<?php

	session_start();
	
	if (isset($_POST['email']))
	{
		
		$all_right=true;
		$nick = $_POST['nick'];
		
		
		if ((strlen($nick)<3) || (strlen($nick)>20))
		{
			$all_right=false;
			$_SESSION['e_nick']="Nick musi posiadać od 3 do 20 znaków!";
		}
		
			if (ctype_alnum($nick)==false)
			{
				$all_right=false;
				$_SESSION['e_nick']="Nick może składać się tylko z liter i cyfr (bez polskich znaków)";
			}
		
		
		$email = $_POST['email'];
		$emailB = filter_var($email, FILTER_SANITIZE_EMAIL);
		
		if ((filter_var($emailB, FILTER_VALIDATE_EMAIL)==false) || ($emailB!=$email))
		{
			$all_right=false;
			$_SESSION['e_email']="Podaj poprawny adres e-mail!";
		}
		
		
		$haslo1 = $_POST['haslo1'];
		$haslo2 = $_POST['haslo2'];
		
		if ((strlen($haslo1)<8) || (strlen($haslo1)>20))
		{
			$all_right=false;
			$_SESSION['e_haslo']="Hasło musi posiadać od 8 do 20 znaków!";
		}
		
			if ($haslo1!=$haslo2)
			{
				$all_right=false;
				$_SESSION['e_haslo']="Podane hasła nie są identyczne!";
			}	

		$haslo_hash = password_hash($haslo1, PASSWORD_DEFAULT);
		
		
		if (!isset($_POST['regulamin']))
		{
			$all_right=false;
			$_SESSION['e_regulamin']="Potwierdź akceptację regulaminu!";
		}				
		
		
		$key = "6Lc6oiEaAAAAAFoOX7y6OxHXArO5sLpdhIf5_shT";
		$check = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$key.'&response='.$_POST['g-recaptcha-response']);		
		$odpowiedz = json_decode($check);
		
		if ($odpowiedz->success==false)
		{
			$all_right=true;
			//$_SESSION['e_bot']="Potwierdź, że nie jesteś botem!";
		}		
		
		//Zapamiętaj wprowadzone dane
		$_SESSION['fr_nick'] = $nick;
		$_SESSION['fr_email'] = $email;
		$_SESSION['fr_haslo1'] = $haslo1;
		$_SESSION['fr_haslo2'] = $haslo2;
		if (isset($_POST['regulamin'])) $_SESSION['fr_regulamin'] = true;
		
		require_once "connect.php";
		mysqli_report(MYSQLI_REPORT_STRICT);
		
		try 
		{
			$polaczenie = new mysqli($host, $db_user, $db_password, $db_name);
			if ($polaczenie->connect_errno!=0)
			{
				throw new Exception(mysqli_connect_errno());
			}
			else
			{
				//Czy email już istnieje?
				$result = $polaczenie->query("SELECT id FROM users WHERE email='$email'");
				
				if (!$result) throw new Exception($polaczenie->error);
				
				$how_many = $result->num_rows;
				if($how_many>0)
				{
					$all_right=false;
					$_SESSION['e_email']="Istnieje już konto przypisane do tego adresu e-mail!";
				}		
				$result = $polaczenie->query("SELECT id FROM users WHERE user='$nick'");
				
				if (!$result) throw new Exception($polaczenie->error);
				
				$how_many = $result->num_rows;
					if($how_many>0)
					{
						$all_right=false;
						$_SESSION['e_nick']="Istnieje już gracz o takim nicku! Wybierz inny.";
					}
				
				if ($all_right==true)
				{
					//Hurra, wszystkie testy zaliczone, dodajemy gracza do bazy
					
					if ($polaczenie->query("INSERT INTO users VALUES (12, '$nick', '$haslo_hash', '$email')"))
					{
						$_SESSION['udanarejestracja']=true;
						header('Location: witamy.php');
					}
					else
					{
						throw new Exception($polaczenie->error);
					}
					
				}
				
				$polaczenie->close();
			}
			
		}
		catch(Exception $e)
		{
			echo '<span style="color:red;">Błąd serwera! Przepraszamy za niedogodności i prosimy o rejestrację w innym terminie!</span>';
			echo '<br />Informacja developerska: '.$e;
		}
		
	}
	
	
?>
<!DOCTYPE HTML>
<html>
<head>	
	<title>Strefa Kibica - Jastębie: formularz_rejestracji</title>	
	<?php 
		require_once('headStatic.php');
	?>

	<script src='https://www.google.com/recaptcha/api.js'></script>
	
	<style>
		.error
		{
			color:red;
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>
</head>
	<body>
		<?php 
			require_once('nav.php');
		?>
		
		<form class="form" method="post">
		
			Nazwa użytkownika: <br /> <input type="text" value="<?php
				if (isset($_SESSION['fr_nick']))
				{
					echo $_SESSION['fr_nick'];
					unset($_SESSION['fr_nick']);
				}
			?>" name="nick" /><br />
			
			<?php
				if (isset($_SESSION['e_nick']))
				{
					echo '<div class="error">'.$_SESSION['e_nick'].'</div>';
					unset($_SESSION['e_nick']);
				}
			?>
			
			E-mail: <br /> <input type="text" value="<?php
				if (isset($_SESSION['fr_email']))
				{
					echo $_SESSION['fr_email'];
					unset($_SESSION['fr_email']);
				}
			?>" name="email" /><br />
			
			<?php
				if (isset($_SESSION['e_email']))
				{
					echo '<div class="error">'.$_SESSION['e_email'].'</div>';
					unset($_SESSION['e_email']);
				}
			?>
			
			Wprowadź hasło: <br /> <input type="password"  value="<?php
				if (isset($_SESSION['fr_haslo1']))
				{
					echo $_SESSION['fr_haslo1'];
					unset($_SESSION['fr_haslo1']);
				}
			?>" name="haslo1" /><br />
			
			<?php
				if (isset($_SESSION['e_haslo']))
				{
					echo '<div class="error">'.$_SESSION['e_haslo'].'</div>';
					unset($_SESSION['e_haslo']);
				}
			?>		
			
			Powtórz hasło: <br /> <input type="password" value="<?php
				if (isset($_SESSION['fr_haslo2']))
				{
					echo $_SESSION['fr_haslo2'];
					unset($_SESSION['fr_haslo2']);
				}
			?>" name="haslo2" /><br />
			
			<label>
				<input type="checkbox" name="regulamin" <?php
				if (isset($_SESSION['fr_regulamin']))
				{
					echo "checked";
					unset($_SESSION['fr_regulamin']);
				}
					?>/> Akceptuję regulamin
			</label>
			
			<?php
				if (isset($_SESSION['e_regulamin']))
				{
					echo '<div class="error">'.$_SESSION['e_regulamin'].'</div>';
					unset($_SESSION['e_regulamin']);
				}
			?>	
			
			<div class="g-recaptcha" data-sitekey="6Lc6oiEaAAAAAFoOX7y6OxHXArO5sLpdhIf5_shT"></div>
			
			<?php
				if (isset($_SESSION['e_bot']))
				{
					echo '<div class="error">'.$_SESSION['e_bot'].'</div>';
					unset($_SESSION['e_bot']);
				}
			?>	
			
			<br />
			
			<input type="submit" value="Zarejestruj się" />
			
		</form>

		<?php 
			require_once('footer.php');
		?>
	</body>	
	<?php 
		require_once('scripts.php');
	?>
</html>